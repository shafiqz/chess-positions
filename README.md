# 2 kings, 2 queens, 2 bishops  and 1 knight chess problem
Find the no of unique solutions that can be generated when 2 kings, 2 queens, 2 bishops and 1 knight is placed on a 7x7 chess board.

To run the program, make sure you have sbt on your path environment variable and type `sbt run`.

The test class ChessSpec solve many other kind of scenarios.