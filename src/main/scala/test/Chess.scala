package test

import scala.annotation.tailrec
import scala.collection.Set
import scala.collection.parallel.ParSet

/**
  * Created by saz on 10/7/2016.
  */
object Chess {

  implicit val posOrdering = new PositionOrdering

  /**
    * Check to see if a piece can safely occupy a square on the board.
    * <p>
    *    The piece can occupy the square if the following condition holds:
    *     1. The square has not been occupied by another piece.
    *     2. The square is not under attacked by any already placed pieced.
    *     3. The piece will not threaten any existing pieces when placed.
    *  </p>
    * @param square Square a piece wants to occupy.
    * @param occupied List of positions already occupied by the piece.
    * @param attackable List of squares that is attacked by the pieces of
    *                   occupied
    * @param boardWidth Width of the board
    * @param boardHeight Height of the board.
    * @return True if it is safe to place the piece there. False otherwise.
    */
  def safeToPlace(square: Placement,          occupied: List[Placement],
                  attackable: List[Position], boardWidth: Int,
                  boardHeight: Int) : Boolean = {

    val e = square.piece.movablePositions(square.position, boardWidth,
                                          boardHeight)
    !(//If the square is already occupied, then it's not safe.
      occupied.exists( p => { p.position == square.position }) ||
      //If the square can be attacked by already placed pieces, then it is not safe.
      attackable.contains( square.position ) ||
      //If when placed in the square, the candidate can attack already placed
      //pieces, then it is not safe.
      occupied.exists(p => { e.contains(p.position) }))
  }

  /**
    * Generate a list of piece.
    * The generated List will be in this order: Kings, Queens, Rooks, Bishops,
    * Knights.
    * @param kings No. of kings to generate. Start from 0.
    * @param queens No. of queens to generate. Start from 0.
    * @param rooks No. of rooks to generate. Start from 0.
    * @param bishops No. of bishops to generate. Start from 0.
    * @param knights No. of knights to generate. Start from 0.
    * @return A list of pieces.
    */
  def generatePieces(kings: Int, queens: Int, rooks: Int, bishops: Int,
                     knights: Int): List[Piece] = {

    List.fill(kings)(King) ++
    List.fill(queens)(Queen)++
    List.fill(rooks)(Rook) ++
    List.fill(bishops)(Bishop) ++
    List.fill(knights)(Knight)
  }

  /**
    * Generate the chess-board.
    * @param boardWidth Width of the board.
    * @param boardHeight Height of the board.
    * @return All the positions of the board.
    */
  def generateBoardPositions(boardWidth: Int, boardHeight: Int):
  List[Position] = {
    val rows = List.range(1, boardHeight + 1)
    val cols = List.range(1, boardWidth + 1)

    for {
      col <- cols
      row <- rows
    } yield {Position.create(col, row) }
  }

  def genAttackable(placements: List[Placement], boardWidth:Int,
                    boardHeight:Int): List[Position] = {

    placements.flatMap (placement => {
      val Placement(piece, position) = placement
      piece.movablePositions(position, boardWidth, boardHeight)
    })
  }

  def placePiece(piece: Piece,              occupied: List[Placement],
                 chessBoard: Set[Position], boardWidth: Int,
                 boardHeight: Int): Set[List[Placement]] = {

    val attackable = genAttackable(occupied, boardWidth, boardHeight)

    chessBoard.foldLeft(Set[List[Placement]]()) ((acc, aSquare) =>

      if (safeToPlace(Placement(piece, aSquare), occupied, attackable,
                      boardWidth, boardHeight)) {
          val placements = (Placement(piece, aSquare) :: occupied).sortBy(_.position)
          acc + placements
        } else {
          acc
        }
    )
  }

  def placePiece(piece: Piece,              occupied: ParSet[List[Placement]],
                 chessBoard: Set[Position], boardWidth: Int,
                 boardHeight: Int): ParSet[List[Placement]] = {

    occupied.flatMap(placements =>
      placePiece(piece, placements, chessBoard, boardWidth, boardHeight)
    )
  }

  @tailrec
  final def solve(piecesToPut:List[Piece],   occupied: ParSet[List[Placement]],
                  chessBoard: Set[Position], boardWidth: Int,
                  boardHeight: Int): Set[List[Placement]] = {

    piecesToPut match {
      case Nil => occupied.toList.toSet
      case head :: tail => solve(piecesToPut.tail, placePiece(head, occupied, chessBoard, boardWidth, boardHeight),
                                 chessBoard,       boardWidth, boardHeight)
    }
  }

  def compute(kings: Int,   queens: Int,  rooks: Int,
              bishops: Int, knights: Int, boardWith: Int,
              boardHeight: Int) : Set[List[Placement]] = {

    val boardSquares = generateBoardPositions(boardWith, boardHeight).toSet
    val allPieces    = generatePieces(kings, queens, rooks, bishops, knights)

    solve(allPieces, ParSet(List()), boardSquares, boardWith, boardHeight)
  }
}